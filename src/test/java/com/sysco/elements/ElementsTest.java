package com.sysco.elements;


import com.sysco.functions.Success;
import com.sysco.pages.ElementsPage;
import com.sysco.pages.SuccessPage;
import com.sysco.pages.TestBase;
import com.syscolab.qe.core.reporting.SyscoLabListener;
import org.openqa.selenium.support.PageFactory;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

@Listeners(SyscoLabListener.class)
public class ElementsTest extends TestBase {


    private static final String LINK_TEXT_SUCCESS_MESSAGE = "Link success";
    private static final String RADIO_BUTTON = "male";
    private static final String CHECK_BOX = "Bike";
    private static final String DropDown = "saab";
    private static final String Tabtext = "tab 1 content";
    private static final String TableContent = "tab 1 content";


//    @CacheLookup
//    @FindBy(linkText = "Click me using this link text");
//    WebElement linkTextLnkElement;
//
//
//public ElementsPage(WebDriver driver) {
//    PageFactory.initElements(driver,ElementsPage.class);
//}
//
//public void clickLinkTextLnk () {
//
//    linkTextLnkElement;click();
//    waitUntilElementVisible(By.className(("entry-title");
//}
//@Listeners(SyscoLabListener.class)


@BeforeClass
public void initClass(ITestContext iTestContext) {

        iTestContext.setAttribute("feature", "ultimateqa - UI Automation");
    }

    @Test(description = "Verify success message")
    public void testLinkTextLocator(){

        SoftAssert softAssert = new SoftAssert();

        ElementsPage page = PageFactory.initElements(driver,ElementsPage.class);
        page.clickLinkTextLnk();

        SuccessPage successPage = new SuccessPage();
        String actualLinkTextSuccessMessage = successPage.getSuccessMessageText();

        softAssert.assertEquals(actualLinkTextSuccessMessage,LINK_TEXT_SUCCESS_MESSAGE, "Link text success message incorrect" );
        softAssert.assertAll();
        driver.navigate().back();

    }

    @Test(description = "Verify radio button text" , dependsOnMethods = "testLinkTextLocator")
    public void testRadioButtonLocator(){

        SoftAssert softAssert = new SoftAssert();

        ElementsPage page = PageFactory.initElements(driver,ElementsPage.class);
        page.clickRadioBtn();

        SuccessPage successPage = new SuccessPage();
        String actualLinkTextSuccessMessage = successPage.getSRadioButtonText();

        softAssert.assertEquals(actualLinkTextSuccessMessage,RADIO_BUTTON, "Radio Button text is incorrect" );
        softAssert.assertAll();

    }

    @Test(description = "Verify check box text", dependsOnMethods = "testRadioButtonLocator")
    public void testCheckBoxLocator(){

        SoftAssert softAssert = new SoftAssert();

        ElementsPage page = PageFactory.initElements(driver,ElementsPage.class);
        page.clickCheckBox();

        SuccessPage successPage = new SuccessPage();
        String actualLinkTextSuccessMessage = successPage.getCheckBoxText();

        softAssert.assertEquals(actualLinkTextSuccessMessage,CHECK_BOX, "Check Box text is incorrect" );
        softAssert.assertAll();

    }
    @Test(description = "Verify drop down text", dependsOnMethods = "testCheckBoxLocator")
    public void testDropDownLocator(){

        SoftAssert softAssert = new SoftAssert();

        ElementsPage page = PageFactory.initElements(driver,ElementsPage.class);
        page.clickDropDown();

        SuccessPage successPage = new SuccessPage();
        String actualLinkTextSuccessMessage = successPage.getDropDownText();

        softAssert.assertEquals(actualLinkTextSuccessMessage,DropDown, "Drop Down text is incorrect" );
        softAssert.assertAll();

    }

    @Test(description = "Verify Tab button Text", dependsOnMethods = "testDropDownLocator")
    public void testTabBtnLocator(){

        SoftAssert softAssert = new SoftAssert();

        ElementsPage page = PageFactory.initElements(driver,ElementsPage.class);
        page.clickTabBtn();

        SuccessPage successPage = new SuccessPage();
        String actualLinkTextSuccessMessage = successPage.selectTabText();

        softAssert.assertEquals(actualLinkTextSuccessMessage,Tabtext, "Tab text is incorrect");
        softAssert.assertAll();

    }
    @Test(description = "Verify table content", dependsOnMethods = "testTabBtnLocator")
    public void testTableLocator(){

        SoftAssert softAssert = new SoftAssert();

        softAssert.assertEquals(Success.getTableDetails(),"$50,000+", "QE Salary");
        softAssert.assertAll();

    }
}


