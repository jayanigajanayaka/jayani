package com.sysco.pages;

import org.openqa.selenium.By;


public class ElementsPage extends PageBase {


    private By linkTextLnk = By.linkText("Click me using this link text!");
    private By radioButton = By.xpath("//input[@name='gender' and @type='radio']");
    private By xpathButton = By.xpath("//*[@id='button1']");
    private By checkBox = By.xpath("//input[@name='vehicle' and @type='checkbox']");
    private By dropDown = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select");
    private By tabBtn = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[10]/ul/li[1]/a");

    public void clickLinkTextLnk(){
        driver.findElement(linkTextLnk).click();
        waitUntillElementVisible(By.className("entry-title"));
    }

    public void clickRadioBtn(){
        driver.findElement(radioButton).click();
        waitUntillElementClickable(By.xpath("//input[@name='gender' and @type='radio']"));
    }

    public void clickCheckBox(){

        driver.findElement(checkBox).click();
        waitUntillElementClickable(By.xpath("//input[@name='vehicle' and @type='checkbox']"));
    }

    public void clickDropDown(){

        driver.findElement(dropDown).click();
        waitUntillElementClickable(By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select"));
    }

    public void clickTabBtn(){

        driver.findElement(tabBtn).click();
        waitUntillElementVisible(By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[10]/div/div[1]/div"));
    }

}
