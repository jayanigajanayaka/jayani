package com.sysco.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import java.util.List;

public class SuccessPage extends PageBase {

    private By successMessageText = By.className("entry-title");
    private By successMessageText1 = By.className("et_pb_tab_content");
    private By radioButtonText = By.className("Radio buttons");
    private By radioButton = By.xpath("//input[@name='gender' and @type='radio']");
    private By checkBoxText = By.xpath("//input[@name='vehicle' and @type='checkbox']");
    private By dropDownText = By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select/option[1]");
    private By idTable = By.id("htmlTableId");

    public String getSuccessMessageText() {

        WebElement successMessageElement = driver.findElement(successMessageText);
        String successMessage = successMessageElement.getText();
        return successMessage;

    }


    public String getSRadioButtonText() {

        List<WebElement> radioBtn = driver.findElements(By.xpath("//input[@name='gender' and @type='radio']"));
        for (int i = 0; i < radioBtn.size(); i++) {


            WebElement local_radio = radioBtn.get(i);
            String value = local_radio.getAttribute("value");
            return value;
        }
        return null;
    }


    public String getCheckBoxText() {

        List<WebElement> list = driver.findElements(By.xpath("//input[@name='vehicle' and @type='checkbox']"));
        for (int i = 0; i < list.size(); i++) {

            if ((list.get(i).getAttribute("value").trim()
                    .equalsIgnoreCase("bike"))
                    || (list.get(i).getAttribute("value").trim()
                    .equalsIgnoreCase("car"))) {

                WebElement local_checkBox = list.get(i);
                String value = local_checkBox.getAttribute("value");
                return value;
            }

        }

        return null;
    }

    public String getDropDownText() {


        WebElement dropDown = driver.findElement(By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[9]/div/div/div/select"));

        List<WebElement> options = dropDown.findElements(By.tagName("option"));
        options.get(1).click();

        {

            WebElement dropDown_local = options.get(1);
            String value = dropDown_local.getAttribute("value");
            return value;

        }
    }


    public String getTabText() {


        WebElement element = driver.findElement(By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[10]/ul/li[1]/a"));

        element.sendKeys(Keys.TAB);
        element.sendKeys(Keys.ENTER);
//        String Message = successMessageElement.getText();
//        return Message;
        return null;
    }

    public String selectTabText() {

        //WebElement object = driver.findElement(By.xpath("//*[@id=\"et-boc\"]/div/div[3]/div/div[1]/div[10]/ul/li[1]/a"));
        WebElement object = driver.findElement(By.className("et_pb_tab_content"));
        String successMessage1 = object.getText();
        return successMessage1;
    }


    public String getTableDetails() {
        String rowText = null;
        WebElement table = driver.findElement(idTable);

        List<WebElement> tableRows = table.findElements(By.tagName("td"));

        for (int i = 0; i < tableRows.size(); i++) {
            WebElement row = tableRows.get(i);
             rowText = (row.getText());
        }
        return rowText;
    }
    
    
    public String getSalaryOfTheQE(int jobRowID) {

        WebElement baseTable = driver.findElement(idTable);
        List<WebElement> tableRows = baseTable.findElements(By.tagName("tr"));
        String value =tableRows.get(jobRowID).getText();
        return value;
    }
}