package com.sysco.pages;


import com.sysco.Common.Constants;
import com.sysco.utils.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import com.syscolab.qe.core.reporting.SyscoLabListener;
import com.syscolab.qe.core.reporting.SyscoLabQCenter;
import com.syscolab.qe.core.reporting.SyscoLabReporting;


import org.testng.ITestContext;


import static com.syscolab.qe.core.reporting.SyscoLabListener.getResults;
import static com.syscolab.qe.core.reporting.SyscoLabReporting.generateJsonFile;


public class TestBase {
    private SyscoLabQCenter syscoLabQCenter;
    private SyscoLabReporting syscoLabReporting;
    private SyscoLabListener testListeners;
    protected WebDriver driver;
    @BeforeClass
    public void beforeClass(){

        driver = WebDriverManager.getDriver();
        driver.get("https://www.ultimateqa.com/simple-html-elements-for-automation");
        syscoLabQCenter = new SyscoLabQCenter();
        syscoLabReporting = new SyscoLabReporting();
        testListeners = new SyscoLabListener();

    }


    @AfterClass(alwaysRun = true)
    public void cleanUpBaseClass(ITestContext iTestContext) {
        try {
            if (Constants.UPDATE_DASHBOARD) {
                syscoLabQCenter.setProjectName(Constants.TEST_PROJECT);
                syscoLabQCenter.setEnvironment(Constants.TEST_ENV);
                syscoLabQCenter.setRelease(Constants.TEST_RELEASE);
                syscoLabQCenter.setModule(iTestContext.getAttribute("feature").toString());
                syscoLabQCenter.setFeature(iTestContext.getAttribute("feature").toString());
                syscoLabQCenter.setClassName(iTestContext.getClass().getName());
                generateJsonFile(getResults(), syscoLabQCenter);

            }

            driver.quit();
        } catch (Exception e) {
            e.printStackTrace();
            driver.quit();
        }
    }


}