package com.sysco.pages;


import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class LoginPage extends PageBase {

    private By emailTxt = By.id("user_email");
    private By passwordTxt = By.id("user_password");
    private By signInBtn = By.id("btn-signin");


    public void login(String email, String password){

        WebElement emailtextBox =  driver.findElement(emailTxt);
        emailtextBox.sendKeys(email);
        WebElement passwordtextBox =  driver.findElement(passwordTxt);
        passwordtextBox.sendKeys(password);
        WebElement signInButton = driver.findElement(signInBtn);
        signInButton.click();

    }
}
