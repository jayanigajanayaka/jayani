package com.sysco.pages;

import com.sysco.utils.WebDriverManager;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;


public class PageBase {

    protected WebDriver driver;
    private Wait wait;

    public PageBase(){
        driver = WebDriverManager.getDriver();
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    }

    public WebElement waitForElementToBeClickable(By by){

        wait =  new FluentWait<>(driver)
                .withTimeout(20, TimeUnit.SECONDS)
                .pollingEvery(500, TimeUnit.MILLISECONDS)
                .ignoring(NoSuchElementException.class)
                .ignoring(StaleElementReferenceException.class)
                .ignoring(ElementNotVisibleException.class);


        wait.until(new Function() {
            @Override
            public WebElement apply(Object o) {
                if(driver.findElement(by).getAttribute("value").equals("success")){
                    return driver.findElement(by);
                }
                return null;
            }
        });

        return (WebElement) wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public void waitUntillElementClickable(By by){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.elementToBeClickable(by));
    }

    public void waitUntillElementClickable(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitUntillElementVisible(By by){
        WebDriverWait wait = new WebDriverWait(driver, 40);
        wait.until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitUntillElementVisible(WebElement element){
        WebDriverWait wait = new WebDriverWait(driver, 20);
        wait.until(ExpectedConditions.visibilityOfElementLocated((By) element));
    }

    public int getQESalary() {

        return 0;
    }
}
