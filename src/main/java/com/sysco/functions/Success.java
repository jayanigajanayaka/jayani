package com.sysco.functions;

import com.sysco.pages.SuccessPage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class Success {
    static SuccessPage successPage = new SuccessPage();

    public static String getTableDetails() {
        return successPage.getTableDetails();
    }
}
