package com.sysco.Common;

public class Constants {

        public static final String TEST_ENV = System.getProperty("test.env", "UIAutomation");
        public static final String TEST_RELEASE = System.getProperty("test.release", "Jayani2");
        public static final String TEST_PROJECT = System.getProperty("test.project", "Java Based Web UI Automation");
        public static final boolean UPDATE_DASHBOARD = Boolean.parseBoolean(System.getProperty("update.dashboard", "true"));
        public static final boolean RUN_LOCALLY = Boolean.parseBoolean(System.getProperty("run.locally", "false"));
        public static final String APP_URL = System.getProperty("app.url", "https://www.ultimateqa.com/simple-html-elements-for-automation");
        public static final String APP_OS = System.getProperty("app.os", "WIN10");
        public static final String APP_BROWSER = System.getProperty("app.browser", "chrome");
        public static final Long DEFAULT_TIMEOUT = 15L;

    }

